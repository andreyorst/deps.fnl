# Changelog

## Development

- Fix bug with accessing luarocks dependency non-existent `dependencies` field

## 0.2.3 (02.03.2025)

* Prepend profiles to the main `deps` map, to allow overriding `paths` and `deps`
* Force Git to run in a non-interactive mode
* Add the `--verbosity` flag
* Fix bug with default `deps.fnl` file check
* Support Git dependency URLs in reverse-domain format

## 0.2.2 (16.02.2025)

* Produce PATHs relative to the working directory
* Fix bug where empty `profiles` would prevent `deps` from working

## 0.2.1 (16.02.2025)

* Add `--fennel-ls` flag for configuring `fennel-ls` based on `deps.fnl`
* Add `--profiles` flag for applying profiles from `deps.fnl`
* Fix a bug where `deps` assumed that `deps.fnl` is always present
* Clear all PATHs before configuring

## 0.2.0 (09.02.2025)

* Add `--tree` flag for printing the dependency tree
* Add `sync-deps` function for runtime use
* Install Luarocks dependencies into separate trees
* Change dependency format to a map
* Remove the `--allow-conflicts` flag
* Limit conflict resolution to dependencies on the same depth level
* Resolve conflicts based on exported modules, not just library names and versions
* Allow explicitly overriding dependencies
* Return a list of modules provided by the libraries added with `add-lib` and `add-libs`
* Add `register-backend` as a WIP interface for adding new backends

## 0.1.0 (05.02.2025)

* Change dependency format to a list of vectors
* Use `dofile` on `fennel.lua` instead of `(os.execute fennel)`
* Add `--lua-version` flag to specify Lua version to Luarocks
* Add `add-lib` and `add-libs` for runtime use

## 0.0.1 (08.01.2025)

Initial release of `deps.fnl`
