(require-macros
  :io.gitlab.andreyorst.fennel-test)

(local fennel
  (require :fennel))

(local {: skip-test : eq}
  (require :io.gitlab.andreyorst.fennel-test))

(fn load-deps []
  "A hacky way of requiring the deps script.
Because `deps` is an executable script, we have to replace the shebang
and call to the `main` function with a `do` expression, essentially
transforming deps to `(do ...deps-script... {needed exports})`."
  (with-open [f (io.open "deps" :r)]
    (let [lines (icollect [line (f:lines)] line)]
      ;; replace shebang with do
      (tset lines 1 "(do")
      ;; replace (main) with exports
      (tset lines (length lines)
            "{: merge-deps
          : breadth-first-search
          : breadth-first-iter
          : sha?
          : vector?
          : dirname
          : valid?
          : read-deps
          : git/url-from-dep})")
      (fennel.eval (table.concat lines "\n")))))

(local {: merge-deps
        : breadth-first-search
        : breadth-first-iter
        : sha?
        : vector?
        : dirname
        : valid?
        : read-deps
        : git/url-from-dep}
  (load-deps))

(local { : find} (require :utils))

(deftest merge-deps-test
  (testing "deps merging"
    (assert-eq {} (merge-deps))
    (assert-eq {} (merge-deps nil))
    (assert-eq {} (merge-deps nil nil))
    (assert-eq {:deps {"a" true}} (merge-deps {:deps {"a" true}}))
    (assert-eq {:deps {"a" true}} (merge-deps {:deps {"a" true}} nil))
    (assert-eq {:deps {"a" true}} (merge-deps nil {:deps {"a" true}}))
    (assert-eq {:deps {"a" true "b" true}}
               (merge-deps {:deps {"a" true}} {:deps {"b" true}}))
    (assert-eq {:deps {"a" true "b" true "c" true "d" true}}
               (merge-deps {:deps {"a" true "b" true}} {:deps {}} {:deps {"c" true "d" true}}))
    (assert-eq {:deps {"a" true "b" true "c" true "d" true}}
               (merge-deps {:deps {"a" true "b" true}} {} {:deps {"c" true "d" true}}))
    (assert-eq {:deps {"a" true "b" true "c" true}}
               (merge-deps {:deps {"a" true "b" true}} {:deps {"c" true "c" true}}))
    (assert-eq {:deps {"a" true "b" true "c" true}}
               (merge-deps {:deps {"a" true "b" true}} {:deps {"c" true}} {:deps {"c" true}})))
  (testing "paths merging"
    (assert-eq {:paths {:fennel ["a" "b"] :macro [] :lua ["c"] :clua []}}
               (merge-deps {:paths {:fennel ["a"]}} {:paths {:fennel ["b"] :lua ["c"]}}))
    (assert-eq {:paths {:fennel ["a"] :macro [] :lua ["c"] :clua []}}
               (merge-deps {:paths {:fennel ["a"]}} {:paths {:fennel ["a"] :lua ["c"]}})))
  (testing "wrong paths are ignored"
    (assert-eq {:paths {:clua {} :fennel ["a"] :lua {} :macro {}}}
               (merge-deps {:paths {:fennel ["a"]}} {:paths {:funnel ["b"]}})))
  (testing "full deps file merge"
    (assert-eq {:deps {"a" true "b" true "c" true "d" true}
                :paths {:fennel ["a" "b"]
                        :lua ["c" "d"]
                        :macro ["f"]
                        :clua ["e"]}
                :build "make"}
               (merge-deps {:deps {"a" true "b" true}
                            :paths {:fennel ["a"] :lua ["c"]}
                            :build "make"}
                           {:deps {"c" true "d" true}
                            :paths {:fennel ["b"] :lua ["d"] :clua ["e"]}
                            :build "no make"}
                           {:paths {:macro ["f"]}}))))

(deftest breadth-first-test
  (testing "breadth-first traversal"
    (let [tree [1 [2 [4] [5]] [3 [6]]]]
      (assert-eq [1 2 3 4 5 6]
                 (icollect [[_ [node]] (breadth-first-iter tree vector? (fn [[_ & xs]] xs))]
                   node))))
  (testing "breadth-first search"
    (let [tree [1 [2 [4] [5]] [3 [6]]]]
      (each [search path (pairs {[1 [2 [4] [5]] [3 [6]]] [[1 [2 [4] [5]] [3 [6]]]]
                                 [2 [4] [5]] [[1 [2 [4] [5]] [3 [6]]] [2 [4] [5]]]
                                 [3 [6]] [[1 [2 [4] [5]] [3 [6]]] [3 [6]]]
                                 [4] [[1 [2 [4] [5]] [3 [6]]] [2 [4] [5]] [4]]
                                 [5] [[1 [2 [4] [5]] [3 [6]]] [2 [4] [5]] [5]]
                                 [6] [[1 [2 [4] [5]] [3 [6]]] [3 [6]] [6]]})]
        (assert-eq path
                   (breadth-first-search tree
                                         (fn [node]
                                           (eq node search))
                                         vector?
                                         (fn [[_ & xs]] xs))
                   (fennel.view search))))))

(deftest sha-test
  (testing "valid SHAs pass"
    (assert-is (sha? "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"))
    (assert-is (sha? "75ef008f1105746307d3a3cfa638b4a037e9c60b")))
  (testing "invalid SHAs detected"
    (assert-not (sha? "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"))
    (assert-not (sha? "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaag"))))


(deftest basedir-test
  (testing "common basedir shienanigans"
    (assert-eq "/foo/bar"
               (dirname "/foo/bar/baz"))
    (assert-eq "/foo/bar"
               (dirname "/foo/bar/baz/"))
    (assert-eq "/"
               (dirname "/"))))

(fn filename [path]
  (pick-values 1 (path:gsub ".+/" "")))

(deftest valid-test
  (case (_G._VERSION:match "^Lua *(.+)")
    :5.1 (skip-test "skipping valid test on lua5.1")
    :5.2 (skip-test "skipping valid test on lua5.2"))
  (fn invalid? [deps-path reason]
    (let [(ok? deps) (pcall read-deps deps-path)
          _ (assert-is ok? (.. "failed to read " deps-path " " (tostring deps)))
          (ok? msg) (pcall valid? deps (filename deps-path))]
      (assert-not ok? (.. "deps file " deps-path " is valid"))
      (assert-is (find (tostring msg) reason) (tostring msg))))
  (testing "deps.fnl alidation"
    (invalid? "test/data/incorrect/deps-not-a-map.fnl"
              "deps-not-a-map.fnl did not conform to spec: 'deps' field is not an associative table")
    (invalid? "test/data/incorrect/git-dep-not-string.fnl"
              "git-dep-not-string.fnl did not conform to spec: 'deps' field is not an associative table")
    (invalid? "test/data/incorrect/git-sha-empty.fnl"
              "git-sha-empty.fnl did not conform to spec: sha for git dependency \"https://gitlab.com/andreyorst/deps.fnl\" is epmty")
    (invalid? "test/data/incorrect/paths-string.fnl"
              "paths-string.fnl did not conform to spec: 'paths' field is not an associative table")
    (invalid? "test/data/incorrect/rock-dep-not-string.fnl"
              "rock-dep-not-string.fnl did not conform to spec: 'deps' field is not an associative table")
    (invalid? "test/data/incorrect/rock-version-empty.fnl"
              "rock-version-empty.fnl did not conform to spec: version tag for dependency \"dep\" is empty")
    (invalid? "test/data/incorrect/nested-rock-version-empty.fnl"
              "nested-rock-version-empty.fnl did not conform to spec: version tag for dependency \"dep2\" is empty")
    (invalid? "test/data/incorrect/deps-string.fnl"
              "deps-string.fnl did not conform to spec: 'deps' field is not an associative table")))

(deftest git-forges
  (testing "forge"
    (testing "github"
      (assert-eq
       "https://github.com/foo/bar.git"
       (git/url-from-dep "com.github.foo/bar"))
      (assert-eq
       "https://github.com/foo/bar.git"
       (git/url-from-dep "io.github.foo/bar")))
    (testing "gitlab"
      (assert-eq
       "https://gitlab.com/foo/bar.git"
       (git/url-from-dep "com.gitlab.foo/bar"))
      (assert-eq
       "https://gitlab.com/foo/bar.git"
       (git/url-from-dep "io.gitlab.foo/bar")))
    (testing "bitbucket"
      (assert-eq
       "https://bitbucket.org/foo/bar.git"
       (git/url-from-dep "org.bitbucket.foo/bar"))
      (assert-eq
       "https://bitbucket.org/foo/bar.git"
       (git/url-from-dep "io.bitbucket.foo/bar")))
    (testing "beanstalkapp"
      (assert-eq
       "https://foo.git.beanstalkapp.com/bar.git"
       (git/url-from-dep "com.beanstalkapp.foo/bar"))
      (assert-eq
       "https://foo.git.beanstalkapp.com/bar.git"
       (git/url-from-dep "io.beanstalkapp.foo/bar")))
    (testing "sourcehut"
      (assert-eq
       "https://git.sr.ht/~foo/bar"
       (git/url-from-dep "ht.sr.foo/bar")))
    (testing "codeberg"
      (assert-eq
       "https://codeberg.org/foo/bar.git"
       (git/url-from-dep "com.codeberg.foo/bar")))))
