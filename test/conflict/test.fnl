(require-macros
  :io.gitlab.andreyorst.fennel-test)

(local {: run : find : copy-deps : make-tmp-git : tmpdir}
  (require :utils))

(use-fixtures :once
  (fn [t]
    (copy-deps "test/conflict/.deps")
    (make-tmp-git ".deps/git/A/A/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
    (make-tmp-git ".deps/git/B/B/bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb")
    (make-tmp-git ".deps/git/C/C/cccccccccccccccccccccccccccccccccccccccc")
    (make-tmp-git ".deps/git/C/C/dddddddddddddddddddddddddddddddddddddddd")
    (t)))

(deftest conflict-test
  (testing "transient Git libraries conflict on the same depth level"
    (let [{: out : ok?}
          (run (.. "./deps --lua ${LUA:-lua} --fennel-path --deps-file test/conflict/deps.fnl --deps-dir " (tmpdir) "/.deps"))]
      (assert-is (not ok?) out)
      (assert-is
       (or (find out "[A/A:aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa]->[C/C:cccccccccccccccccccccccccccccccccccccccc] conflicts with [B/B:bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb]->[C/C:dddddddddddddddddddddddddddddddddddddddd]")
           (find out "[B/B:bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb]->[C/C:dddddddddddddddddddddddddddddddddddddddd] conflicts with [A/A:aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa]->[C/C:cccccccccccccccccccccccccccccccccccccccc]"))
       out))))

(deftest allow-conflicts-test
  (testing "root level overrides transient Git libraries"
    (let [{: out : ok?}
          (run (.. "./deps --lua ${LUA:-lua} --fennel-path --deps-file test/conflict/deps-fixed.fnl --deps-dir " (tmpdir) "/.deps"))
          _ (assert-is ok? out)
          path (out:match "FENNEL_PATH=\"(.+)\"")
          paths (collect [s (string.gmatch path "([^;\n]+)")] s true)]
      (assert-eq {(.. (tmpdir) "/.deps/git/A/A/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/src/?.fnl") true
                  (.. (tmpdir) "/.deps/git/B/B/bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb/src/?.fnl") true
                  (.. (tmpdir) "/.deps/git/C/C/cccccccccccccccccccccccccccccccccccccccc/src/?.fnl") true
                  (.. (tmpdir) "/.deps/git/C/C/dddddddddddddddddddddddddddddddddddddddd/src/?.fnl") true
                  "test/conflict/src/?.fnl" true}
                 paths))))
