(require-macros
  :io.gitlab.andreyorst.fennel-test)

(local {: skip-test}
  (require :io.gitlab.andreyorst.fennel-test))

(local {: run}
  (require :utils))

(use-fixtures :each
  (fn [t]
    (run "rm -rf test/real/.deps/")
    (t)
    (run "rm -rf test/real/.deps/")))

(deftest real-test
  (testing "real-world deps.fnl file is processed correctly"
    (when (os.getenv :SKIP_LONG_TESTS)
      (skip-test "skipping long tests"))
    (let [{: out : ok?}
          (run "./deps --lua ${LUA:-lua} --fennel-path --deps-file test/real/deps.fnl --deps-dir test/real/.deps")
          _ (assert-is ok? out)
          path (out:match "FENNEL_PATH=\"(.+)\"")
          paths (collect [s (string.gmatch path "([^;\n]+)")] s true)]
      (assert-eq {"test/real/.deps/git/andreyorst/json.fnl/eebcb40750d6f41ed03fed04373f944dcf297383/src/?.fnl" true
                  "test/real/.deps/git/andreyorst/reader.fnl/252ea2474cb7399020e6922f700a5190373e6f98/src/?.fnl" true
                  "test/real/src/?.fnl" true
                  "test/real/src/?/init.fnl" true}
                 paths))))
