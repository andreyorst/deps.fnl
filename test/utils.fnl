(local {: view}
  (require :fennel))

(local {: skip-test}
  (require :io.gitlab.andreyorst.fennel-test))

(local utils {})

(fn utils.run [program ...]
  "Run a `program` and capture it's output, exit code, success status and
optional error message."
  (let [command (-> (fcollect [i 1 (select :# ...) :into [program]]
                      (-?> i (select ...) (view {:one-line? true})))
                    (table.concat " ")
                    (.. " 2>&1"))
        proc (io.popen command)
        out (proc:read :*a)
        (ok? message code) (proc:close)]
    {: ok? : out : message : code}))

(fn utils.find [s pattern]
  (case (s:find pattern 1 true)
    (start end) (s:sub start end)))

(fn utils.copy-deps [deps-dir]
  (utils.run (.. "rm -rf " (utils.tmpdir) "/.deps"))
  (case (utils.run (.. "cp -r " deps-dir " " (.. (utils.tmpdir) "/.deps")))
    {: out :ok? false} (error out 2)))

(fn utils.make-tmp-git [path]
  (when (os.getenv :SKIP_GIT_TESTS)
    (skip-test "skipping git tests"))
  (let [path (.. (utils.tmpdir) "/" path)]
    (case (utils.run (.. "git -C " path " init "
                         "&& git -C " path " add . "
                         "&& git -C " path " commit -m init"))
      {: out :ok? false} (error out 2))))

(fn utils.tmpdir []
  (or (os.getenv :TMPDIR) "/tmp"))

utils
