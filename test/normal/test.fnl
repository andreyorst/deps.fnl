(require-macros
  :io.gitlab.andreyorst.fennel-test)

(local {: run : copy-deps : make-tmp-git : tmpdir}
  (require :utils))

(use-fixtures :once
  (fn [t]
    (copy-deps "test/normal/.deps")
    (make-tmp-git ".deps/git/A/A/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
    (make-tmp-git ".deps/git/B/B/bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb")
    (make-tmp-git ".deps/git/C/C/cccccccccccccccccccccccccccccccccccccccc")
    (t)))

(deftest normal-test
  (testing "deps.fnl file loads fine"
    (let [{: out : ok?}
          (run (.. "./deps --lua ${LUA:-lua} --fennel-path --deps-file test/normal/deps.fnl --deps-dir " (tmpdir) "/.deps"))
          _ (assert-is ok? out)
          path (out:match "FENNEL_PATH=\"(.+)\"")
          paths (collect [s (string.gmatch path "([^;\n]+)")] s true)]
      (assert-eq {(.. (tmpdir) "/.deps/git/A/A/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/src/?.fnl") true
                  (.. (tmpdir) "/.deps/git/B/B/bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb/src/?.fnl") true
                  (.. (tmpdir) "/.deps/git/C/C/cccccccccccccccccccccccccccccccccccccccc/src/?.fnl") true
                  "test/normal/src/?.fnl" true}
                 paths))))
