(require-macros
  :io.gitlab.andreyorst.fennel-test)

(local {: skip-test}
  (require :io.gitlab.andreyorst.fennel-test))

(local {: run : find : copy-deps : make-tmp-git : tmpdir}
  (require :utils))

(use-fixtures :once
  (fn [t]
    (copy-deps "test/rock-conflict/.deps")
    (make-tmp-git ".deps/git/A/A/badc0debadc0debadc0debadc0debadc0debadc0")
    (make-tmp-git ".deps/git/B/B/badc0debadc0debadc0debadc0debadc0debadc0")
    (t)))

(deftest rock-conflict-test
  (testing "transient Luarocks libraries conflict on the same depth level"
    (when (os.getenv :SKIP_LONG_TESTS)
      (skip-test "skipping long tests"))
    (let [{: out : ok?}
          (run (.. "./deps --lua ${LUA:-lua} --fennel-path --deps-file test/rock-conflict/deps.fnl --deps-dir " (tmpdir) "/.deps"))]
      (assert-is (not ok?) out)
      (assert-is
       (or (find out "[A/A:badc0debadc0debadc0debadc0debadc0debadc0]->[bump:3.1.7-1] conflicts with [B/B:badc0debadc0debadc0debadc0debadc0debadc0]->[bump:3.1.6-2]")
           (find out "[B/B:badc0debadc0debadc0debadc0debadc0debadc0]->[bump:3.1.6-2] conflicts with [A/A:badc0debadc0debadc0debadc0debadc0debadc0]->[bump:3.1.7-1]"))
       out))))

(deftest allow-conflicts-test
  (testing "root level overrides transient Luarocks libraries"
    (when (os.getenv :SKIP_LONG_TESTS)
      (skip-test "skipping long tests"))
    (let [{: out : ok?}
          (run (.. "./deps --lua ${LUA:-lua} --fennel-path --deps-file test/rock-conflict/deps-fixed.fnl --deps-dir " (tmpdir) "/.deps"))
          _ (assert-is ok? out)
          path (out:match "FENNEL_PATH=\"(.-)\"")
          paths (collect [s (string.gmatch path "([^;\n]+)")] s true)]
      (assert-eq {(.. (tmpdir) "/.deps/git/A/A/badc0debadc0debadc0debadc0debadc0debadc0/?.fnl") true
                  (.. (tmpdir) "/.deps/git/B/B/badc0debadc0debadc0debadc0debadc0debadc0/?.fnl") true
                  "test/rock-conflict/src/?.fnl" true}
                 paths))
    (let [lua-ver (_G._VERSION:match "^Lua *(.+)")
          {: out : ok?}
          (run "luarocks" "--lua-version" lua-ver "--tree" (.. (tmpdir) "/.deps/rocks/bump/3.1.7-1") "show" "bump" "3.1.7-1")]
      (assert-is ok? out)
      (assert-is
       (find out "bump 3.1.7-1")
       out))
    (let [lua-ver (_G._VERSION:match "^Lua *(.+)")
          {: out : ok?}
          (run "luarocks" "--lua-version" lua-ver "--tree" (.. (tmpdir) "/.deps/rocks/bump/3.1.6-2") "show" "bump" "3.1.6-2")]
      (assert-is ok? out)
      (assert-is
       (find out "bump 3.1.6-2")
       out))))
