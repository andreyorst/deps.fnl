(require-macros
  :io.gitlab.andreyorst.fennel-test)

(local {: skip-test}
  (require :io.gitlab.andreyorst.fennel-test))

(local deps
  (require :deps))

(fn Set [t]
  (collect [_ x (ipairs t)] x true))

(deftest adding-git-lib
  (when (os.getenv :SKIP_LONG_TESTS)
    (skip-test "skipping long tests"))
  (testing "Git dependency returns a correct module list after processing"
    (assert-eq
     (Set ["io.gitlab.andreyorst.json"
           "io.gitlab.andreyorst.reader"])
     (Set (deps.add-lib
           "https://gitlab.com/andreyorst/json.fnl"
           {:type :git :sha "eebcb40750d6f41ed03fed04373f944dcf297383"})))))

(deftest adding-luarocks-lib
  (when (os.getenv :SKIP_LONG_TESTS)
    (skip-test "skipping long tests"))
  (testing "Luarocks dependency returns a correct module list after processing"
    (assert-eq
     (Set ["bump"])
     (Set (deps.add-lib "bump" {:type :rock :version "3.1.7-1"}))))
  (testing "Luarocks installation fails without transient dependencies"
    (assert-not (pcall deps.add-lib "penlight" {:type :rock :version "1.14.0-2"}))
    (assert-not (pcall deps.add-lib
                       "lapis"
                       {:type :rock :version "1.16.0-1"
                        :dependencies {"loadkit" {:type :rock :version "1.1.0-1"}
                                       ;; missing etlua"
                                       "luasocket" {:type :rock :version "3.1.0-1"}
                                       "lua-cjson" {:type :rock :version "2.1.0.10-1"}
                                       "argparse" {:type :rock :version "0.7.1-1"}
                                       "lpeg" {:type :rock :version "1.1.0-2"}
                                       "ansicolors" {:type :rock :version "1.0.2-3"}
                                       "date" {:type :rock :version "2.2.1-1"}
                                       "pgmoon" {:type :rock :version "1.16.0-1"
                                                 :dependencies {"lpeg" {:type :rock :version "1.1.0-2"}}}
                                       "luaossl" {:type :rock :version "20220711-0"}}}))
    (assert-not (pcall deps.add-lib
                       "lapis"
                       {:type :rock :version "1.16.0-1"
                        :dependencies {"loadkit" {:type :rock :version "1.1.0-1"}
                                       "etlua" {:type :rock :version "1.3.0-1"}
                                       "luasocket" {:type :rock :version "3.1.0-1"}
                                       "lua-cjson" {:type :rock :version "2.1.0.10-1"}
                                       "argparse" {:type :rock :version "0.7.1-1"}
                                       "lpeg" {:type :rock :version "1.1.0-2"}
                                       "ansicolors" {:type :rock :version "1.0.2-3"}
                                       "date" {:type :rock :version "2.2.1-1"}
                                       "pgmoon" {:type :rock :version "1.16.0-1"}
                                       ;; missing lpeg
                                       "luaossl" {:type :rock :version "20220711-0"}}})))
  (testing "Luarocks dependency also returns modules from transient dependencies"
    (assert-eq
     (Set ["pl.Date" "pl.comprehension" "pl.List" "pl.OrderedMap" "pl.Map" "pl.sip" "pl.tablex" "pl.dir"
           "pl.stringx" "pl.stringio" "pl.Set" "pl.MultiMap" "pl.types" "pl.lapp" "pl.xml" "pl.utils"
           "pl.url" "pl.luabalanced" "pl.text" "pl.test" "pl.data" "pl.template" "pl.file" "pl.seq"
           "pl.operator" "pl.app" "pl.path" "pl.strict" "pl.permute" "pl.pretty" "pl.class" "pl.lexer"
           "pl.config" "pl.array2d" "pl.init" "pl.input" "pl.compat" "pl.func" "pl.import_into" "lfs"])
     (Set (deps.add-lib "penlight" {:type :rock :version "1.14.0-2" :dependencies {"luafilesystem" {:type :rock :version "1.8.0-1"}}}))))
  (testing "Luarocks dependency with multi-layer transient dependencies"
    (assert-eq
     (Set ["lapis.db.base_model" "lapis.nginx.http" "lapis.nginx.postgres" "lapis.cmd.templates.migration" "lapis.cmd.templates.nginx.config"
           "lapis.db.sqlite.schema" "lapis.cmd.path" "lapis.db.postgres.model" "lapis.request" "lapis.cmd.templates.gitignore"
           "lapis.cmd.templates.models" "lapis.csrf" "lapis.init" "lapis.nginx" "lapis.db.schema"
           "lapis.logging" "lapis.db.migrations" "lapis.util.utf8" "lapis.cache" "lapis.lua"
           "lapis.server" "lapis.spec.stack" "lapis.cmd.templates.rockspec" "lapis.coroutine" "lapis.moonscript.widget_optimizer"
           "lapis.cmd.templates.tupfile" "lapis.cmd.nginx.attached_server" "lapis.db.sqlite" "lapis.views.error" "lapis.spec.server"
           "lapis.cmd.cqueues.attached_server" "lapis.db" "lapis.etlua" "lapis.application.route_group" "lapis.html"
           "lapis.cmd.cqueues" "lapis.db.postgres.schema" "lapis.cmd.nginx" "lapis.version" "lapis.db.base"
           "lapis.validate" "lapis.nginx.cache" "lapis.db.sqlite.model" "lapis.util" "lapis.cmd.actions"
           "lapis.db.postgres.parse_clause" "lapis.cmd.templates.flow" "lapis.cmd.cqueues.actions" "lapis.nginx.context" "lapis.util.fenv"
           "lapis.views.layout" "lapis.validate.types" "lapis.db.postgres" "lapis.util.functions" "lapis.http"
           "lapis.router" "lapis.cmd.nginx.config" "lapis.cmd.templates.nginx.mime_types" "lapis.util.encoding" "lapis.spec.shared"
           "lapis.spec.request" "lapis.db.mysql" "lapis.cmd.templates.config" "lapis.session" "lapis.flow"
           "lapis.environment" "lapis.time" "lapis.cqueues" "lapis.application" "lapis.db.model.relations"
           "lapis.db.model" "lapis.config" "lapis.cmd.attached_server" "lapis.db.mysql.schema" "lapis.db.mysql.model"
           "lapis.cmd.templates.spec" "lapis.cmd.util" "lapis.spec" "lapis.db.pagination" "lapis.cmd.templates.model"
           "lapis.features.etlua" "lapis.spec.db" "lapis.cmd.nginx.actions" "lapis.cmd.templates.application" "openssl.x509.chain"
           "openssl.cipher" "openssl.auxlib" "openssl.pkcs12" "_openssl" "openssl.pubkey"
           "openssl.x509.verify_param" "openssl.x509.store" "openssl.pkey" "openssl.x509.name" "openssl.x509.extension"
           "openssl.hmac" "openssl.ocsp.basic" "openssl" "openssl.x509.crl" "openssl.x509.csr"
           "openssl.kdf" "openssl.x509.altname" "openssl.x509" "openssl.des" "openssl.ssl.context"
           "openssl.digest" "openssl.ocsp.response" "openssl.ssl" "openssl.rand" "openssl.bignum"
           "cjson.util" "lua2json" "cjson" "json2lua" "socket.tp" "ltn12" "socket" "socket.core" "socket.http" "mime"
           "socket.url" "socket.serial" "socket.smtp" "mime.core" "socket.unix" "socket.ftp" "socket.headers" "etlua" "loadkit"
           "ansicolors" "argparse" "date" "pgmoon.json" "pgmoon.hstore" "pgmoon.init" "pgmoon.bit" "pgmoon.util" "pgmoon.arrays"
           "pgmoon.cqueues" "pgmoon.crypto" "pgmoon.socket" "re" "lpeg" "re" "lpeg"])
     (Set (deps.add-lib
           "lapis"
           {:type :rock :version "1.16.0-1"
            :dependencies {"loadkit" {:type :rock :version "1.1.0-1"}
                           "etlua" {:type :rock :version "1.3.0-1"}
                           "luasocket" {:type :rock :version "3.1.0-1"}
                           "lua-cjson" {:type :rock :version "2.1.0.10-1"}
                           "argparse" {:type :rock :version "0.7.1-1"}
                           "lpeg" {:type :rock :version "1.1.0-2"}
                           "ansicolors" {:type :rock :version "1.0.2-3"}
                           "date" {:type :rock :version "2.2.1-1"}
                           "pgmoon" {:type :rock :version "1.16.0-1"
                                     :dependencies {"lpeg" {:type :rock :version "1.1.0-2"}}}
                           "luaossl" {:type :rock :version "20220711-0"}}})))))
