(require-macros
  :io.gitlab.andreyorst.fennel-test)

(local {: skip-test}
  (require :io.gitlab.andreyorst.fennel-test))

(local {: run : find}
  (require :utils))

(deftest no-paths-test
  (case (_G._VERSION:match "^Lua *(.+)")
    :5.1 (skip-test "skipping valid test on lua5.1")
    :5.2 (skip-test "skipping valid test on lua5.2"))
  (testing "deps.fnl with no paths for a Git dependency fails to load"
    (let [{: out : ok?}
          (run (.. "./deps --lua ${LUA:-lua} --fennel-path --deps-file test/non-deps-git-library/deps.fnl --deps-dir test/non-deps-git-library/.deps"))]
      (assert-is (not ok?) out)
      (assert-is
       (find out "error listing git test/non-deps-git-library")
       out))))

(deftest wrong-paths-test
  (case (_G._VERSION:match "^Lua *(.+)")
    :5.1 (skip-test "skipping valid test on lua5.1")
    :5.2 (skip-test "skipping valid test on lua5.2"))
  (testing "incorrectly set up deps.fnl fails to load"
    (let [{: out : ok?}
          (run (.. "./deps --lua ${LUA:-lua} --fennel-path --deps-file test/non-deps-git-library/deps-wrong-paths.fnl --deps-dir test/non-deps-git-library/.deps"))]
      (assert-is (not ok?) out)
      (assert-is
       (find out "error listing git test/non-deps-git-library")
       out))))

(deftest wrong-path-variable-test
  (case (_G._VERSION:match "^Lua *(.+)")
    :5.1 (skip-test "skipping valid test on lua5.1")
    :5.2 (skip-test "skipping valid test on lua5.2"))
  (testing "incorrectly set up deps.fnl loads because Git can't distinguish between Lua and Fennel search patterns"
    (let [{: out : ok?}
          (run (.. "./deps --lua ${LUA:-lua} --fennel-path --deps-file test/non-deps-git-library/deps-wrong-path-variable.fnl --deps-dir test/non-deps-git-library/.deps"))
          _ (assert-is ok? out)
          path (out:match "FENNEL_PATH=\"(.-)\"")
          paths (collect [s (string.gmatch path "([^;\n]+)")] s true)]
      (assert-eq {"test/non-deps-git-library/.deps/git/andreyorst/lua-inst/4696470d0ce12d4f5796811f3a069e0006c376a1/?.lua" true}
                 paths))))

(deftest with-paths-test
  (case (_G._VERSION:match "^Lua *(.+)")
    :5.1 (skip-test "skipping valid test on lua5.1")
    :5.2 (skip-test "skipping valid test on lua5.2"))
  (testing "deps.fnl with correct paths loads properly"
    (let [{: out : ok?}
          (run (.. "./deps --lua ${LUA:-lua} --path --deps-file test/non-deps-git-library/deps-fixed.fnl --deps-dir test/non-deps-git-library/.deps"))
          _ (assert-is ok? out)
          path (out:match "LUA_PATH=\"(.-)\"")
          paths (collect [s (string.gmatch path "([^;\n]+)")] s true)]
      (assert-eq {"test/non-deps-git-library/.deps/git/andreyorst/lua-inst/4696470d0ce12d4f5796811f3a069e0006c376a1/?.lua" true}
                 paths))))
