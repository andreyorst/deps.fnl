# `deps.fnl`

`deps` is a dependency and `PATH` manager for Fennel.

## Usage

`deps` is a script that is meant to be executed in the context of a project with the `deps.fnl` file.
The `deps.fnl` file describes the dependencies of the said project, how it should be built, and how its `PATH` should be constructed.

After constructing the PATH it acts as the `fennel` executable, so it is possible to use it as a drop-in replacement for all Fennel-related commands:

    $ deps -c file.fnl

In contrast, with plain `fennel` this could have been:

    $ fennel --add-fennel-path some/path/?.fnl --add-package-path some/path/?.lua --add-package-path some/path/?/init.lua ... -c file.fnl

The `deps` script automates dependency installation and `PATH` calculation.

Available flags:

- `--deps-file` - path to a specific deps file.
  Defaults to `deps.fnl`.
- `--deps-dir` - path to an alternative deps storage.
  Defaults to `.deps`.
- `--merge` - merge the default deps-file with an additional deps file.
- `--profiles` - merge in the additional [profiles](#profiles).
- `--path` - construct `LUA_PATH`, `LUA_CPATH`, `FENNEL_PATH`, and `FENNEL_MACRO_PATH` environment variables.
  The variables are printed to the standard out, and can be evaluated in the shell with `eval $(deps --path)`.
- `--fennel-path` - construct only the `FENNEL_PATH` and `FENNEL_MACRO_PATH` environment variables.
- `--show` - print resulting deps.fnl after processing all arguments.
- `--tree` - print a dependency tree.
- `--lua-version` - specify alternative Lua version for Luarocks commands.
  Version is detected automatically, but can be overridden with this flag.
  On some systems, the Luarocks package defaults to lua5.1 (or another version of Lua), which can cause errors when using a different Lua runtime version.
- `--verbosity` - change logging verbosity.
  Available logging levels: `info`, `warn`, `error`, and `silent`.
  Defaults to `info`.
- `--version` - display deps script version.
-  `--help` - displays the help message.

**Note**: `fennel` executable is required to be on system `PATH` for `deps` to work.

## `deps.fnl` format

The `deps.fnl` file can contain the following fields:

- `deps` - a map of project dependencies, each being either a Git or Luarocks dependency;
- `paths` - a table of project-specific paths needed to correctly load project modules;

The dependency format for a Git dependency is `URL {:type :git :sha "full commit SHA"}`.
The `URL` in the dependency is any URL accepted by `git`.
Luarocks dependencies are formatted as `rock-name {:type :rock :version "version string"}`.

For example:

```clojure
{:deps {"https://gitlab.com/andreyorst/json.fnl"
        {:type :git :sha "fe12faf0e49053ff3dcbc1fdddfedd70db9eee08"}
        "https://gitlab.com/andreyorst/fennel-test"
        {:type :git
         :sha "01ea080dc8176512c5890462252c5bf214baf137"
         :paths {:fennel ["?.fnl"] :macro ["?.fnl"]}}
        "luasocket"
        {:type :rock :version "3.1.0-1"}}
 :paths {:fennel ["src/?.fnl" "src/?/init.fnl"]}}
```

Dependencies are processed recursively, thus if any of the `git` dependencies has its own `deps.fnl` stored in the repository root it will be analyzed and processed.

All dependencies are installed locally to your project into the `.deps` directory at the project's root.

### `:deps`

Each dependency must specify a `type` key, which can either be `git` or `rock`.

#### Git dependencies

Git dependencies are resolved via the Git URL specified as the dependency name.

Each `git` dependency must provide the `sha` key to specify the checkout target.
The `sha` key is a full 40 digit SHA of the commit in the repository.

An optional `paths` table can be provided for dependencies that don't have a `deps.fnl` file.
The `paths` key acts the same as the [`paths`](#paths) key in the `deps.fnl` file, and specifies path configuration specific to the library.

Git URL can be written in a reverse-domain notation for supported Git forges, and `deps` will try to convert this reverse-domain notation to a proper URL.
The following reverse-domain notations are supported:

- [Sourcehut](https://sourcehut.org/):
  `ht.sr.user/repo` → `https://git.sr.ht/~user/repo`
- [Codeberg](https://codeberg.org/):
  `com.codeberg.user/repo` → `https://codeberg.org/user/repo.git`
- [GitHub](https://github.com):
  `com.github.user/repo`, `io.github.user/repo` → `https://github.com/user/repo.git`
- [GitLab](https://gitlab.com):
  `com.gitlab.user/repo`, `io.gitlab.user/repo` → `https://gitlab.com/user/repo.git`
- [Bitbucket](https://bitbucket.org/):
  `org.bitbucket.user/repo`, `io.bitbucket.user/repo` → `https://bitbucket.org/user/repo.git`
- [Beanstalkapp](https://beanstalkapp.com/):
  `com.beanstalkapp.user/repo`, `io.beanstalkapp.user/repo` → `https://user.git.beanstalkapp.com/repo.git`

Patches for other forges are welcome.

#### Luarocks dependencies

Luarocks dependencies are resolved against their names in the [root Manifest](https://luarocks.org/m/root)

Each `rock` dependency must specify a `version` to install.
If the rock has transient dependencies of its own, these dependencies must be specified under the `:dependencies` key:

```clojure
{:deps {"penlight"
        {:type :rock :version "1.14.0-2"
         :dependencies
         {"luafilesystem"
          {:type :rock :version "1.8.0-1"}}}}}
```

The `deps` script will raise an error on missing transient dependencies.

The reason for this is that Luarocks transient dependencies usually don't specify an exact dependency version.
This introduces problems with conflict resolution, and determinism of what dependencies the project would use on fresh checkout.

### `:paths`

An optional `paths` key can be present and it must be a table with either one of the following keys:

- `fennel` - for setting up the `FENNEL_PATH` environment variable
- `lua` - for setting up the `LUA_PATH` environment variable
- `clua` - for setting up the `LUA_CPATH` environment variable
- `macro` - for setting up the `FENNEL_MACRO_PATH` environment variable

After processing all of the dependencies all paths are collected and set.
Paths obtained from `git` dependencies take higher priority than paths from Luarocks, *because*.

### `:profiles`

The `profiles` key can hold nested `deps` tables.
For example, if your project reqires additional dependencies during development, like a testing framework, it can be put under the `:dev` profile:

```clojure
{:paths {:fennel ["src/?.fnl"]}
 :profiles
 {:dev {:deps {"https://gitlab.com/andreyorst/fennel-test"
               {:type :git :sha "9aae4dd52be1f5dd1ee97e61c639e6c5a2f9afee"}}
        :paths {:fennel ["test/?.fnl"]}}}}
```

This profile can be used via the `--profiles` flag:

```
$ deps --profiles dev run-tests
```

Multiple profiles can be specified separated by commas:

```
$ deps --profiles dev,test
```

### Building dependencies

The `deps.fnl` file can contain a special field `build` that specifies how to build the project after downloading.
It is recommended to keep this field simple, and instead call something that does the building, like `make` or a custom script.

For example, if the library's entry point is `src/core.fnl` but the library should be built to Lua for a more convenient usage, the `deps.fnl` file could contain a command to call `fennel` to compile the file:

```clojure
{:path {:lua ["src/?.lua"]}
 :build "fennel --require-as-include -c src/core.fnl > src/core.lua"}
```

When processing such library, `deps` will ask if the script should be executed:

```clojure
$ deps --repl
processing git repo: https://gitlab.com/andreyorst/deps-build-example
depenency "andreyorst/deps-build-example" requires executing a build script:
fennel --require-as-include -c src/core.fnl > src/core.lua
Proceed? [y/n]: y
Welcome to Fennel 1.5.1-dev on PUC Lua 5.4!
Use ,help to see available commands.
>> (local core (require :core))
nil
>> core
{:greet #<function: 0x55fb2a7cbed0>}
```

If the user chooses not to execute the script, the library won't be built and the `deps` script will exit with a non-zero code.

## Interactive use

When `deps` starts the REPL, a new module is added to the `package.preload`:

```clojure
(local deps (require :deps))
```

The module exposes three functions: `sync-deps`, `add-lib` and `add-libs`.

The `sync-deps` function can be used to add new dependenciss to the PATH after modifying the `deps.fnl` file.

The `add-lib` function provides a way to add dependencies at runtime without requiring application restart:

```clojure
(deps.add-lib "bump" {:type :rock :version "3.1.7-1"})
```

This will download, build, and add `bump` to the `PATH`, making it available for `require`.

The `add-libs` function is similar, except accepting a sequential table with several library coordinates in it:

```clojure
(deps.add-libs {"bump" {:type :rock :version "3.1.7-1"}
                "anim8" {:type :rock :version "v2.3.1-1"}})
```

## PATH resolution in case of version conflicts

After installing and processing all of the dependencies and dependency dependencies the `deps` script constructs the appropriate PATHs for each variable/flag.
By default, version conflicts will throw an error.

For example, if a project depends on two libraries:

```clojure
;; project's deps.fnl
{:deps {"https://github.com/user/A" {:type :git :sha "aaa"}
        "https://github.com/user/B" {:type :git :sha "bbb"}}
 :paths {:fennel ["src/?.fnl"]}}
```

Both `A` and `B` have a dependency on `C` but of different versions:

```clojure
;; A's deps.fnl
{:deps {"https://github.com/user/C" {:type :git :sha "ccc"}}
 :paths {:fennel ["src/?.fnl"]}}

;; B's deps.fnl
{:deps {"https://github.com/user/C" {:type :git :sha "ddd"}}
 :paths {:fennel ["src/?.fnl"]}}
```

Invoking `deps` would produce the following message:

```sh
$ deps --fennel-path
[user/A:aaa]->[user/C:ccc] conflicts with [user/B:bbb]->[user/C:ddd] on module: c
```

Meaning, that the top-level dependency `A` conflicts with another top levdl dependency `B` due to a differen4 version of a transient dependency  dependency `C`.

This conflict can be resolved by specifying the transient dependency as a non-transient one:

```clojure
;; project's deps.fnl
{:deps {"https://github.com/user/A" {:type :git :sha "aaa"}
        "https://github.com/user/B" {:type :git :sha "bbb"}
        "https://github.com/user/C" {:type :git :sha "ccc"}}
 :paths {:fennel ["src/?.fnl"]}}
```

This will force the use of dependency `C` of version `ccc`.

## Development

This project uses the [Fennel Style Guide](https://fennel-lang.org/style).

Tests are done via the [fennel-test](https://gitlab.com/andreyorst/fennel-test) library.
To run the tests execute the following command:

```
./deps tasks/run-tests
```

The `deps` script will download and install the `fennel-test` library, and run tests.

Docs are generated with [Fenneldoc](https://gitlab.com/andreyorst/fenneldoc) and can be found [here](https://gitlab.com/andreyorst/deps.fnl/-/blob/main/doc/deps/deps.md).
