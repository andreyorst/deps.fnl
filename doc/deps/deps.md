# Deps (0.2.4)
`deps` - a dependency and `PATH` manager for Fennel.

This file contains all of the functions defined in the script, and should be treated as a documentation for developers, not a general documentation for users.
For user documentation see the [readme](../README.md) and the `--help` message.

**Table of contents**

- [`Error`](#error)
- [`basename`](#basename)
- [`branch?`](#branch)
- [`breadth-first-iter`](#breadth-first-iter)
- [`breadth-first-search`](#breadth-first-search)
- [`build-paths`](#build-paths)
- [`check-conflicts`](#check-conflicts)
- [`children`](#children)
- [`configure-fennel-ls`](#configure-fennel-ls)
- [`deepcopy`](#deepcopy)
- [`deps.add-lib`](#depsadd-lib)
- [`deps.add-libs`](#depsadd-libs)
- [`deps.sync-deps`](#depssync-deps)
- [`dirname`](#dirname)
- [`empty?`](#empty)
- [`fatal-error`](#fatal-error)
- [`file-exists?`](#file-exists)
- [`function?`](#function)
- [`generate-env-variables`](#generate-env-variables)
- [`git.process`](#gitprocess)
- [`git.validate`](#gitvalidate)
- [`git/available?`](#gitavailable)
- [`git/build-dep`](#gitbuild-dep)
- [`git/modules`](#gitmodules)
- [`git/parse-url`](#gitparse-url)
- [`git/tree`](#gittree)
- [`git/url-from-dep`](#giturl-from-dep)
- [`git/url-to-project-name`](#giturl-to-project-name)
- [`git/url?`](#giturl)
- [`help`](#help)
- [`into`](#into)
- [`log`](#log)
- [`luarocks.process`](#luarocksprocess)
- [`luarocks.validate`](#luarocksvalidate)
- [`luarocks/available?`](#luarocksavailable)
- [`luarocks/dependencies`](#luarocksdependencies)
- [`luarocks/modules`](#luarocksmodules)
- [`luarocks/path`](#luarockspath)
- [`luarocks/rock-installed?`](#luarocksrock-installed)
- [`luarocks/tree`](#luarockstree)
- [`main`](#main)
- [`map?`](#map)
- [`merge`](#merge)
- [`merge-deps`](#merge-deps)
- [`merge-profiles`](#merge-profiles)
- [`merge-with`](#merge-with)
- [`pat-escape`](#pat-escape)
- [`path`](#path)
- [`path->env-storage`](#path-env-storage)
- [`process-command-line-args`](#process-command-line-args)
- [`process-deps`](#process-deps)
- [`read-deps`](#read-deps)
- [`register-backend`](#register-backend)
- [`run`](#run)
- [`setup-paths`](#setup-paths)
- [`sha?`](#sha)
- [`show-deps`](#show-deps)
- [`show-tree`](#show-tree)
- [`show-tree*`](#show-tree-1)
- [`spit`](#spit)
- [`string?`](#string)
- [`table?`](#table)
- [`update-vals`](#update-vals)
- [`valid-deps?`](#valid-deps)
- [`valid-paths?`](#valid-paths)
- [`valid?`](#valid)
- [`vector?`](#vector)
- [`view-path`](#view-path)

## `Error`
Function signature:

```
(Error format-string ...)
```

An error wrapper to prevent stack trace from appearing in the message.
Accepts a `format-string` and its arguments.  Stack traces are
meaningless, as these are user-level errors, there's no need to link
to the `deps` internal implementation.

## `basename`
Function signature:

```
(basename path)
```

Return a filename part of the given file `path`.

## `branch?`
Function signature:

```
(branch? node)
```

Determine if `node` is a branch by checking for non-empty
`dependencies` fileld.

## `breadth-first-iter`
Function signature:

```
(breadth-first-iter tree branch? children)
```

Stateful breadth-first iterator.
Accepts `tree` and two functions to traverse the tree.  The `branch?`
function must take one argument, and determine if the argument is a
node that may or may not have children.  The `children` function must
accept one argument and return a, possible empty, sequential table of
branch children.  The `children` function is only called on a node if
the `branch?` function returns `true` with this node passed as an
argument.

## `breadth-first-search`
Function signature:

```
(breadth-first-search tree test branch? children)
```

Breadth-first search over a given `tree`.
The `test` function which must take a single argument and return
`true` if (test node) matchs the search criteria.  The `branch?`
function must take one argument, and determine if the argument is a
node that may or may not have children.  The `children` function must
accept one argument and return a, possible empty, sequential table of
branch children.  The `children` function is only called on a node if
the `branch?` function returns `true` with this node passed as an
argument.  Returns the full path up to the searched item.

## `build-paths`
Function signature:

```
(build-paths deps-tree)
```

Build path table from `deps-tree`.

## `check-conflicts`
Function signature:

```
(check-conflicts deps-tree)
```

Check if `deps-tree` is free of conflicts.

## `children`
Function signature:

```
(children {:dependencies dependencies})
```

Obtain node `dependencies` sequence.

## `configure-fennel-ls`
Function signature:

```
(configure-fennel-ls paths)
```

Create or update configuration of fennel-ls based on `paths` information.

## `deepcopy`
Function signature:

```
(deepcopy x)
```

Create a deep copy of a given table `x`, producing immutable tables
for nested tables.

## `deps.add-lib`
Function signature:

```
(deps.add-lib name dep-spec)
```

Add a single library `name` to the main deps file and it's `dep-spec`.
`dep-spec` is an associative table with the dependency type and
version.

## `deps.add-libs`
Function signature:

```
(deps.add-libs coords)
```

Add multiple libraries to the main deps file specified as `coords`.
`coords` is a sequential table of library coordinates.  A library
coordinate is a sequential table, with the dependency name, and its
description map.

## `deps.sync-deps`
Function signature:

```
(deps.sync-deps)
```

Calls `add-libs` with any librariess present in `deps.fnl` but not yet
present on the path.

## `dirname`
Function signature:

```
(dirname path)
```

Return a base directory of the given `path`.

## `empty?`
Function signature:

```
(empty? x)
```

Check if `x` is either an empty string or an empty table.

## `fatal-error`
Function signature:

```
(fatal-error message code)
```

If the deps script is running in the REPL context, throws an ordinary
error with a `message`.  Otherwise logs the error `message` to
`stderr` and exits with the specified `code`.

## `file-exists?`
Function signature:

```
(file-exists? filename)
```

Check if `filename` exists on the filesystem.

## `function?`
Function signature:

```
(function? f)
```

Check if `f` is a callable function.

## `generate-env-variables`
Function signature:

```
(generate-env-variables paths)
```

Generate appropriate environment variables for the shell based on
`paths` obtained from `build-paths`.

## `git.process`
Function signature:

```
(git.process url {:paths paths :sha sha} process-deps)
```

Process a Git dependency by cloning the given `url`.
The `sha` key specifies the checkout target.  If the repository
contains its own `deps.fnl` file, process it recursively.  If the
dependency spec contains the `paths` key, prepends its value to the
resulting `paths` field.  Uses `process-deps` to recursively process
any specified dependencies.

## `git.validate`
Function signature:

```
(git.validate url {:paths paths :sha sha} valid?)
```

Validate the dependency `url`, its `sha` and `paths` field.
The `paths` field is validated with the `valid?` callback.

## `git/available?`
Function signature:

```
(git/available?)
```

A portable way to check if Luarocks is installed.

## `git/build-dep`
Function signature:

```
(git/build-dep name lib-path)
```

Ask whether it is OK to build a dependency `name` and execute the
script in the `lib-path` directory.

## `git/modules`
Function signature:

```
(git/modules lib-path all-paths)
```

List modules of a Git library at `lib-path` based on `all-paths`
collected from the `deps.fnl` file.

## `git/parse-url`
Function signature:

```
(git/parse-url url)
```

Parses a Git `url` string.
Supports HTTP and SSH protocols.  Returns a table with `scheme`,
`host`, `port`, `userinfo`, and `path` fields from the URL.

## `git/tree`
Function signature:

```
(git/tree)
```

Git-specific root for dependencies.

## `git/url-from-dep`
Function signature:

```
(git/url-from-dep name)
```

Construct URL from `name` which is a reverse-domain identifier.
Supported forges are listed in `git/services`.

## `git/url-to-project-name`
Function signature:

```
(git/url-to-project-name url)
```

If the `url` is a plain URL, tries to parse it and extract project
name from it off the path component.  If the `url` argument is a
reverse-domain identifier, tries to convert it to apporpriate URL, and
uses the orgiginal `url` argument as the project name.

## `git/url?`
Function signature:

```
(git/url? s)
```

Check if `s` is a valid URL.

## `help`
Function signature:

```
(help)
```

Display a help message specific to the deps script.

## `into`
Function signature:

```
(into to from)
```

Collect items from sequential table `from` into sequential table `to`.

## `log`
Function signature:

```
(log out ...)
```

Write arguments into the `out` as a log message.

## `luarocks.process`
Function signature:

```
(luarocks.process name {:dependencies dependencies :version version} process-deps)
```

Install a rock of the given `name` and `version`.
If the rock has `dependencies` field, process only the explicitly
required dependencies specified by the rock manifest via the
`process-deps` callback.  If any of the requested dependencies are
missing in the `dependencies` table an error is raised.

## `luarocks.validate`
Function signature:

```
(luarocks.validate name {:dependencies dependencies :version version} valid?)
```

Validate the denendency `name`, it's `version` and any possible `dependencies`.
The `dependencies` are validated with the `valid?` callback.

## `luarocks/available?`
Function signature:

```
(luarocks/available?)
```

A portable way to check if Luarocks is installed.

## `luarocks/dependencies`
Function signature:

```
(luarocks/dependencies rock version)
```

Get a table of dependencies required by the given `rock` of the given
`version` stored in the `tree`.

## `luarocks/modules`
Function signature:

```
(luarocks/modules rock version)
```

Get a table of modules exported by the given `rock` of the given
`version` stored in the `tree`.

## `luarocks/path`
Function signature:

```
(luarocks/path rock version)
```

Obtain PATH information for the given `rock` of the given `version` from Luarocks.

## `luarocks/rock-installed?`
Function signature:

```
(luarocks/rock-installed? rock version)
```

Check if the specified `rock` of the given `version` is already
installed.

## `luarocks/tree`
Function signature:

```
(luarocks/tree name version)
```

Construct a Luarocks-specific root for dependency `name` of the given `version`.

## `main`
Function signature:

```
(main)
```

The main intry point.

Populates the `options` by processes the command line arguments and,
depending on the options table either shows help, paths, or passes the
arguments unaffected by the `process-command-line-args` to the
`fennel` executable.

## `map?`
Function signature:

```
(map? x)
```

Check if `x` is an associative table specifically.

## `merge`
Function signature:

```
(merge t1 t2)
```

Returns a new table containing items from `t1` and `t2`, overriding
values from `t1` if same keys are present.

## `merge-deps`
Function signature:

```
(merge-deps ...)
```

Merge multiple deps tables into a single one.
The merging rules are as follows:

The dependencies under the `deps` key are appended into the first deps argument:

```fennels
(merge-deps {:deps {a {...}}} {:deps {b {...}}})
;; => {:deps {a {...} b {...}}}
(merge-deps {:deps {a {...} b {...}}} {:deps {}} {:deps {c {...} d {...}}})
;; => {:deps {a {...} b {...} c {...} d {...}}}
```

The paths under the `paths` key are appened for each path type
separately.  Missing path types are created:

```fennels
(merge-deps {:paths {:fennel [a]}} {:paths {:fennel [b] :lua [c]}})
;; => {:paths {:fennel [a b] :macro [] :lua [c] :clua []}}
```

Other keys are not merged from the subsequent deps.

## `merge-profiles`
Function signature:

```
(merge-profiles deps profiles)
```

Merge the specified `profiles` with `deps` map.

## `merge-with`
Function signature:

```
(merge-with f t1 t2)
```

Returns a new table containing items from `t1` and `t2`, if same keys
are present merge is done by calling `f` with both values.

## `pat-escape`
Function signature:

```
(pat-escape s)
```

Escape string `s` to be safely used as a part of a pattern.

## `path`
Function signature:

```
(path ...)
```

Construct file path from arguments.
All `nil` arguments are ignored.

## `path->env-storage`
Function signature:

```
(path->env-storage path)
```

Convert the `path` key into the environment variable or runtime
storage.

## `process-command-line-args`
Function signature:

```
(process-command-line-args)
```

Process command-line arguments, and populate an options table for deps
script.

## `process-deps`
Function signature:

```
(process-deps deps file)
```

Process the given `deps` map. The optional `file` argument indicates
the file from which the `deps` map originated.

This function loops over all dependencies under the `:deps` key and
downloads each dependency based on its `type` key.

## `read-deps`
Function signature:

```
(read-deps deps-file)
```

Read the `deps-file` in a sandbox.
The `table` and `string` modules are provided for manipulation of the
`deps.fnl` contents upon loading it.

## `register-backend`
Function signature:

```
(register-backend backend)
```

Register `backend` for processing dependencies.

The `backend` argument must be a table with keys `type`, `process`,
and `validate`.

The `type` key is a string name for the backend that appears in
dependency description under the `type` key.

The `process` field must be function that accepts the dependency name,
its description map, and the a callback to process nested
dependencies. The `process` function must return a map with the
following keys:

- `lib` - a meaningful library name
- `version` - library's version tag.
- `modules` - an associative table of library modules
- `paths` - an associatibe table of paths for each module type:
  `fennel`, `macro`, `lua`, `clua`.
  Each field is a sequential table of string paths, for example:
- `dependencies` - a sequential table of any transient dependencies.
  The elements of this table are the same as the outer map.

For example:

```fennels
{:lib "andreyorst/json.fnl"
 :version "0acd16a5b0b2b9bc97f591c0fde17c52278d0bc5"
 :modules {:io.gitlab.andreyorst.json true}
 :paths {:fennel ["git/andreyorst/json.fnl/0acd16a5b0b2b9bc97f591c0fde17c52278d0bc5/src/?.fnl"]}
 :dependencies [{:lib "andreyorst/reader.fnl"
                 :version "c630afe4255cdc60145df32825d2ff3be687f169"
                 :modules {:io.gitlab.andreyorst.reader true}
                 :paths {:fennel ["git/andreyorst/reader.fnl/c630afe4255cdc60145df32825d2ff3be687f169/src/?.fnl"]}
                 :dependencies []}]}
```

The `validate` field must be a function that accepts the dependency
name, its description map, and the validation callback to validate any
nested dependencies.  The `validate` function must throw an error,
indicating the reason why the dependency is invalid.


## `run`
Function signature:

```
(run program ...)
```

Run a `program` and capture its output, exit code, success status, and
optional error message.

## `setup-paths`
Function signature:

```
(setup-paths paths)
```

Assign runtime values for paths based on `paths`
obtained from `build-paths`.

All paths are reset prior to setup, to exclude any external paths that
may come from environment.

## `sha?`
Function signature:

```
(sha? x)
```

Check if `x` is a full 40 character Git SHA.

## `show-deps`
Function signature:

```
(show-deps deps)
```

Show the resulting `deps` map after processing all of the options.

## `show-tree`
Function signature:

```
(show-tree deps-tree)
```

Display the resulting dependency tree after processing all of the options.
Accepts the `deps-tree`, generated by the `process-deps` function.

## `show-tree*`
Function signature:

```
(show-tree* deps-tree)
```

Collect the resulting dependency tree.
Accepts the `deps-tree`, generated by the `process-deps` function.

## `spit`
Function signature:

```
(spit file data)
```

Write `data` to the `file`.

## `string?`
Function signature:

```
(string? x)
```

Check if `x` is a string.

## `table?`
Function signature:

```
(table? x)
```

Check if `x` is a table.

## `update-vals`
Function signature:

```
(update-vals map f)
```

Update values of the `map` by invoking `f` on each value.
Returns a new `map`.

## `valid-deps?`
Function signature:

```
(valid-deps? deps deps-file valid?)
```

Validate the `deps` field of the given `deps-file`. Passes the
`valid?` callback into backends that wish to validate dependencies.

## `valid-paths?`
Function signature:

```
(valid-paths? paths deps-file)
```

Validate the `paths` field of the given `deps-file`.

## `valid?`
Function signature:

```
(valid? deps-map deps-file)
```

Validate the `deps-file`.
Analyzes the `paths` and `deps` keys from the given `deps-map`.

## `vector?`
Function signature:

```
(vector? x)
```

Check if `x` is a sequential table specifically.

## `view-path`
Function signature:

```
(view-path dep deps-tree)
```

Provide string representation of a path from the root to the `dep` on
the `deps-tree`.


<!-- Generated with Fenneldoc v1.0.1
     https://gitlab.com/andreyorst/fenneldoc -->
